# matrix-spotdl
## Description
A tiny bot that reads incoming matrix messages with Spotify URLs and downloads the songs/playlists to some folder using spotdl.

An example use case is to run it on a [navidrome server](https://github.com/navidrome/navidrome/releases) and point the bot's output directory to navidrome's music directory.
## Usage
- Login with some client to the bot's account, and start a DM with your private account. Log out again.
- Do the setup described below
- Send open.spotify.com URLs to the bot in the DM room you created, it will attempt to download them and in case of failure send you the error output. Sending a message with the pattern `https://www.youtube.com/xxx|https://open.spotify.com/xxx` will download audio from the specified youtube link and only use the metadata from spotify.

## Setup
This setup guide is meant for Linux systems using systemd. The executable will also work on other systems, just do not forget to give the config file path as first and only command line parameter to the executable.

1. Make sure you have
  - A directory to download the audio to
  - A directory for the bot to store its state
  - A matrix account for the bot nobody else uses (the bot will react to *any* incoming message!)
  - A system user the bot can run as. Grant this user read+write access to the two directories and the config file.
2. Install ffmpeg and spotdl (e.g. by doing `apt install ffmpeg python3-pip` and `pip install spotdl`).
3. Copy `config.json` and the executable to some place on the system (paths and names do not matter, you will specify the full path in the service file). Fill out all necessary fields as described in the sample config file.
4. Copy the service file to `/etc/systemd/system/matrix-spotdl.service`. Fill out all necessary fields (esp. user, group, executable path and config file path)
5. Enable the service to start it on system boot: `systemctl daemon-reload`, `systemctl enable matrix-spotdl.service`

## Troubleshooting
If you get some error output about a non-compatible glibc version, you may need to compile this bot on that system to have the correct library links.

## Building
You can either
- build it on your system. The build requires (besides a working rust toolchain) system packages for `openssl`, `openssl-devel` / `libssl-dev`, `cmake`, and `pkg-config`, _or_
- build it inside a docker container containing a working toolchain for debian bullseye by running `bash build.sh` or ``bash build.sh --release. The build output will then reside in `release-target`.

## Contributing
Feel free to open issues here or in the matrix room #matrix-spotdl:a-0.me
