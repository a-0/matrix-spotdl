FROM rust:bullseye

RUN apt-get update && apt-get upgrade -y
# Install program dependencies
RUN apt-get install -y openssl cmake pkg-config

RUN mkdir -p /opt/build

COPY . /opt/build

WORKDIR /opt/build

CMD cargo build --release
