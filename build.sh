#!/bin/bash
docker build . -t build-matrix-spotdl-release
docker run -v $(pwd)/release-target:/opt/build/target -v -it build-matrix-spotdl-release cargo build $1