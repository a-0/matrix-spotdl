use std::process::{Command, Output};


pub fn run_command(binary_location: &str, output_dir: &str, num_threads: &u32, command: &str, args: &[&str]) -> Output {
    let output = Command::new(binary_location)
        .arg("--print-errors")
        .arg("--output")
        .arg(output_dir)
        .arg("--threads")
        .arg(num_threads.to_string())
        .arg(command)
        .args(args)
        .output()
        .expect("Failed to execute spotdl");

    println!("{}", String::from_utf8_lossy(&output.stdout));
    println!("{}", String::from_utf8_lossy(&output.stderr));

    output
}