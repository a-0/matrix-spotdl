use std::sync::Arc;
use itertools::Itertools;
use matrix_sdk::{
    config::SyncSettings, reqwest::Url, room::{Joined, Room
        }, ruma::{api::client::message::send_message_event, events::{
        reaction::{ReactionEventContent, Relation}, room::{
            message::{MessageType, OriginalRoomMessageEvent, OriginalSyncRoomMessageEvent, RoomMessageEventContent, TextMessageEventContent},
            topic::RoomTopicEventContent
        }}}, Client
    };

use crate::{state::{StateManager, Subscription}, Config, Secrets};


pub async fn init(config: &Config, secrets: &Secrets, state_manager: Arc<StateManager>) -> tokio::task::JoinHandle<()> {
    // Build the matrix client
    let hs_url = Url::parse(&config.homeserver).expect("Failed to parse hs url from client storage");
    let client = Client::builder()
        .homeserver_url(hs_url)
        .sled_store(&config.account_storage_path, None)
        .expect("Client builder returned error")
        .build()
        .await
        .expect("Client creation failed");

    if let Some(session) = state_manager.session() {
        client.restore_login(session.to_owned()).await.expect("Restoring session failed.");
    }
    else {
        client
        .login_username(
            &config.localpart,
            &secrets.password
        ).send().await.expect("Username login failed");

        state_manager.set_session(client.session());
    }
    
    // Actual event handling
    let config_copy = config.clone();
    let state_manager_copy = state_manager.clone();
    client.add_event_handler(
        move |event: OriginalSyncRoomMessageEvent, room: Room, client: Client| {
            event_callback(
                config_copy.spotdl_cmd.clone(),
                config_copy.output.clone(),
                config_copy.num_threads,
                state_manager_copy.clone(),
                event,
                room,
                client
            )
        },
    );

    return tokio::spawn(async move {
        client.sync(SyncSettings::default()).await.expect("Matrix sync failed.");
    });
}

pub async fn event_callback(spotdl_cmd: String, spotdl_output: String, num_threads: u32, state_manager: Arc<StateManager>, event: OriginalSyncRoomMessageEvent, room: Room, client: Client) {
    if let MessageType::Text(TextMessageEventContent { body: msg_body, .. }) = &event.content.msgtype {
        if let Room::Joined(room_joined) = room {
            // Handle edge cases
            if event.sender == client.user_id().unwrap() {
                return;
            }

            let args: Vec<&str> = msg_body.split_whitespace().collect();

            if let Some(command) = args.get(0) {
                let args = &args[1..];
                match *command {
                    "!sub" => {
                        if let Some(subcommand) = args.get(0) {
                            let args = &args[1..];
                            match *subcommand {
                                "add" => {
                                    if args.len() != 2 {
                                        send_plain(&room_joined, "usage: !sub add name link").await;
                                        return;
                                    }
                                    state_manager.add_subscription(Subscription {
                                        name: args[0].to_string(),
                                        url: args[1].to_string(),
                                    });
                                    send_react(event.clone(), &room_joined, "✅").await;
                                    return;
                                },
                                "rm" => {
                                    if args.len() != 1 {
                                        send_plain(&room_joined, "usage: !sub rm name").await;
                                        return;
                                    }
                                    let subs = state_manager.subscriptions();
                                    let text = subs.iter().map(|sub| sub.name.to_string()).format("\n").to_string();
                                    send_plain(&room_joined, &text).await;
                                    return;
                                },
                                "ls" => {
                                    if args.len() != 0 {
                                        send_plain(&room_joined, "usage: !sub ls").await;
                                        return;
                                    }
                                    state_manager.remove_subscription(args[0]);
                                    send_react(event.clone(), &room_joined, "✅").await;
                                    return;
                                },
                                _ => {
                                    send_plain(&room_joined, "You need to select one of the supported subcommands: add, rm, ls.").await;
                                }
                            }
                        }
                    },
                    "!download" => {
                        if args.len() < 1 {
                            send_plain(&room_joined, "usage: !download <link> <spotdl-flags>").await;
                            return;
                        }
                        let resp = send_react(event.clone(), &room_joined, "⌛").await;
                        // Download audio
                        let output = super::spotdl::run_command(&spotdl_cmd, &spotdl_output, &num_threads, "download", args);
                        room_joined.redact(&resp.event_id, None, None).await.expect("Failed to redact reaction");
            
                        send_html(
                            &room_joined,
                            &String::from_utf8_lossy(&output.stdout).to_string(),
                        &(String::from("Output from stdout\n<pre><code>")
                                + &String::from_utf8_lossy(&output.stdout)
                                + "</code></pre>"
                                + "\nError output from stderr:\n"
                                + "<pre><code>"
                                + &String::from_utf8_lossy(&output.stderr)
                                + "</code></pre>")
                        ).await;
                    },
                    _ => {
                        send_plain(&room_joined, "Your message should have started with a command prefixed with '!'. Available commands: sub, download.").await;
                    }
                }
            } else {
                send_plain(&room_joined, "Your message should have started with a command prefixed with '!'. Available commands: sub, download.").await;
            }

            // If no room topic is set, set it to the link of this repository.
            match room_joined.topic() {
                Some(s) => {
                    if s.len() == 0 {
                        set_room_topic(&room_joined).await;
                    }
                }
                None => {
                    set_room_topic(&room_joined).await;
                }
            }
        }
    }
}

async fn send_reply_plain(
    event: OriginalSyncRoomMessageEvent,
    room: &Joined,
    text: &str,
) {
    let casted_event = OriginalRoomMessageEvent {
        content: event.content,
        event_id: event.event_id,
        origin_server_ts: event.origin_server_ts,
        room_id: room.room_id().to_owned(),
        sender: event.sender,
        unsigned: event.unsigned,
    };
    if let Err(e) = room.send(RoomMessageEventContent::text_plain(text).make_reply_to(&casted_event), None).await {
        println!("Failed to send message {}\n{:?}", &text, e);
    }
}

async fn send_plain(
    room: &Joined,
    text: &str,
) {
    if let Err(e) = room.send(RoomMessageEventContent::text_plain(text), None).await {
        println!("Failed to send message {}\n{:?}", &text, e);
    }
}

async fn send_reply_html(
    event: OriginalSyncRoomMessageEvent,
    room: &Joined,
    text_html: &str,
    text_plain: &str,
) {
    let casted_event = OriginalRoomMessageEvent {
        content: event.content,
        event_id: event.event_id,
        origin_server_ts: event.origin_server_ts,
        room_id: room.room_id().to_owned(),
        sender: event.sender,
        unsigned: event.unsigned,
    };
    if let Err(e) = room.send(RoomMessageEventContent::text_html(text_plain, text_html).make_reply_to(&casted_event), None).await {
        println!("Failed to send message {}\n{:?}", &text_html, e);
    }
}

async fn send_html(
    room: &Joined,
    text_html: &str,
    text_plain: &str,
) {
    if let Err(e) = room.send(RoomMessageEventContent::text_html(text_plain, text_html), None).await {
        println!("Failed to send message {}\n{:?}", &text_html, e);
    }
}

async fn send_react(event: OriginalSyncRoomMessageEvent, room: &Joined, content: &str) -> send_message_event::v3::Response {
    let reaction = ReactionEventContent::new(Relation::new(
        (&event.event_id).to_owned(),
        content.to_string(),
    ));
    
    room.send(reaction, None)
        .await
        .expect("Could not send reaction even.")
}

async fn set_room_topic(room: &Joined) {
    let set_topic_event_content = RoomTopicEventContent::new(String::from(
        "Feedback, FAQ & Guides: https://gitlab.com/a-0/matrix-spotdl",
    ));
    if let Err(e) = room.send_state_event(set_topic_event_content).await {
        println!("Error setting room topic: {:?}", e);
    }
}