use serde::{Deserialize, Serialize};
use std::{fs, path::PathBuf, sync::Arc, time::Duration, env};

mod matrix;
mod spotdl;
mod state;
use state::*;


#[derive(Serialize, Deserialize, Clone)]
pub struct Config {
    homeserver: String,
    localpart: String,
    account_storage_path: String,
    spotdl_cmd: String,
    output: String,
    num_threads: u32,
    subscription_update_timer: u64,
}


#[derive(Serialize, Deserialize)]
pub struct Secrets {
    password: String,
}


#[tokio::main]
async fn main() {
    // Get one command line parameter
    let args: Vec<String> = env::args().collect();
    if args.len() != 4 {
        panic!("Error: Three arguments expected. Usage: matrix-spotdl <path_to_cfg_file> <path_to_secrets_file> <path_to_state_dir>");
    }
    let conf_path = PathBuf::from(&args[1]);
    let secrets_path = PathBuf::from(&args[2]);

    // Parse config file
    let conf: Config = read_json(&conf_path).expect("Could not parse config file as json!");
    let secrets: Secrets = read_json(&secrets_path).expect("Could not parse secrets file as json!");
    
    let state_file_path = format!("{}/state.json", args[3]);
    let state_manager = Arc::new(StateManager::new(&state_file_path));

    // If necessary, create the directory for the client's account data
    if !std::path::Path::new(&conf.account_storage_path).is_dir() {
        match std::fs::create_dir_all(&conf.account_storage_path) {
            Ok(_) => (),
            Err(e) => {
                panic!("Failed to create directory for client store! Error: {}", e)
            }
        }
    }

    spawn_subscription_sync(conf.spotdl_cmd.clone(), conf.output.clone(), conf.num_threads, conf.subscription_update_timer, state_manager.clone());

    let matrix_thread_handle = matrix::init(&conf, &secrets, state_manager.clone()).await;
    matrix_thread_handle.await.expect("Error in Matrix thread");
}


fn read_json<T: serde::de::DeserializeOwned>(path: &PathBuf) -> Option<T> {
    fs::read_to_string(&path)
        .ok()
        .and_then(|json_str| serde_json::from_str(&json_str).ok())
}


fn spawn_subscription_sync(bin_path: String, output_dir: String, num_threads: u32, subscription_update_timer: u64, state_manager: Arc<StateManager>) {
    tokio::spawn(async move {
        loop {
            for sub in state_manager.subscriptions() {
                spotdl::run_command(&bin_path, &output_dir, &num_threads, "download", &vec![sub.url.as_str()]);
                tokio::time::sleep(Duration::from_secs(subscription_update_timer * 60)).await;
            }
        }
    });
}
