use std::{collections::HashMap, path::PathBuf, fs};

use matrix_sdk::Session;
use serde::{Serialize, Deserialize};


#[derive(Serialize, Deserialize)]
pub struct Subscription {
    pub url: String,
    pub name: String,
}

#[derive(Serialize, Deserialize, Default)]
pub struct State {
    session: Option<Session>,
    subscriptions: HashMap<String, String>,
}

pub struct StateManager {
    file_path: PathBuf,
}

impl StateManager {
    pub fn new(path: &str) -> Self {
        let state_path: PathBuf = path.into();

        // Create empty state file if none exists
        if !state_path.is_file() {
            std::fs::File::create(state_path).expect("Could not create state file, even though none existed before");
            fs::write(
                Into::<PathBuf>::into(path), 
                serde_json::to_string(&State::default()).expect("Could not create default state for blank state file")
            ).expect("Could not write default state file");
        }

        StateManager { file_path: path.into() }
    }

    pub fn set_session(&self, session: Option<Session>) {
        let mut state: State = super::read_json(&self.file_path).unwrap();
        state.session = session;

        let new_state = serde_json::to_string(&state).expect("Failed to re-convert state to json");
        fs::write(&self.file_path, new_state).unwrap();
    }

    pub fn add_subscription(&self, sub: Subscription) {
        let mut state: State = super::read_json(&self.file_path).unwrap();
        state.subscriptions.insert(sub.name, sub.url);

        let new_state = serde_json::to_string(&state).expect("Failed to re-convert state to json");
        fs::write(&self.file_path, new_state).unwrap();
    }

    pub fn remove_subscription(&self, name: &str) {
        let mut state: State = super::read_json(&self.file_path).unwrap();
        state.subscriptions.remove(name);

        let new_state = serde_json::to_string(&state).expect("Failed to re-convert state to json");
        fs::write(&self.file_path, new_state).unwrap();
    }

    pub fn session(&self) -> Option<Session> {
        let state: State = super::read_json(&self.file_path).unwrap();
        state.session
    }

    pub fn subscriptions(&self) -> Vec<Subscription> {
        let state: State = super::read_json(&self.file_path).unwrap();
        state.subscriptions.iter().fold(vec![], |mut acc, (k, v)| {
            acc.push(Subscription { url: v.to_string(), name: k.to_string() });
            acc
        })
    }
}
