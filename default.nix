{ rustPlatform
, cmake
, pkg-config
, openssl
, ... }:

rustPlatform.buildRustPackage {

  pname = "matrix-spotdl";
  version = "0.7.0";

  src = ./.;

  nativeBuildInputs = [
    cmake
    pkg-config
    openssl
  ];

  PKG_CONFIG_PATH = "${openssl.dev}/lib/pkgconfig";

  cargoLock = {
    lockFile = ./Cargo.lock;
  };
}
