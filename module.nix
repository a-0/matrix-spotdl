{ config
, pkgs
, lib
, ... }:

let

  cfg = config.services.matrix-spotdl;
  matrix-spotdl = (pkgs.callPackage ./default.nix {});

  configFile = pkgs.writeText "matrix-spotdl.json" (builtins.toJSON cfg.config);

in {

  options.services.matrix-spotdl = with lib; {
    enable = mkOption {
      type = types.bool;
      default = false;
      description = "Whether to enable matrix-spotdl";
    };
    user = mkOption {
      type = types.str;
      description = "The system user the bot runs under";
    };
    config = mkOption {
      type = types.submodule {
        options = {
          homeserver = mkOption {
            type = types.str;
            description = "Complete homeserver url including protocol. Something like 'https://matrix.example.com'";
          };
          localpart = mkOption {
            type = types.str;
            description = "localpart of the Matrix ID of the Bot. Matrix IDs look like '@localpart:homeserver.tld'";
          };
          account_storage_path = mkOption {
            type = types.str;
            description = "Absolute path to an empty folder where the bot can store data so it doesn't need to re-sync everything after restart";
          };
          spotdl_cmd = mkOption {
            type = types.str;
            default = "${pkgs.spotdl}/bin/spotdl";
            description = "The package providing spotdl";
          };
          output = mkOption {
            type = types.str;
            description = "The path and template where spotDL should put music files";
          };
          num_threads = mkOption {
            type = types.int;
            description = "Maximum number of threads used for downloading";
          };
          subscription_update_timer = mkOption {
            type = types.int;
            default = 30;
            description = "How often subscriptions shall be updated in minutes";
          };
        };
      };
    };
    secretsFile = mkOption {
      type = types.str;
      default = false;
      description = "Path to bot's secrets file";
    };
    stateDir = mkOption {
      type = types.str;
      default = false;
      description = "Path to bot's state directory";
    };
  };

  config = lib.mkIf cfg.enable {

    systemd.services."matrix-spotdl" = {
      description = "matrix-spotdl | A minimalistic Spotify-downloading bot controlled via [matrix]";
      after = [ "network.target" ];
      wantedBy = [ "multi-user.target" ];

      serviceConfig = {
        ExecStart = "${matrix-spotdl}/bin/matrix-spotdl ${configFile} ${cfg.secretsFile} ${cfg.stateDir}";
        User = cfg.user;
        Type = "simple";
        KillMode = "process";
        Restart = "on-failure";
      };
    };
  };
}
